/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

var change_password = (function(){

  // Elementos del DOM
  var DOM = {
    form_chg_passw: $('#form-chg-passw')  
  };

  var init = function(){
    events.validation();
    events.validation_password();
  };


  var events = {};
  
  events.filter_parameter =  function(val){
    var mayuscu = /[A-Z]/;
    var minuscu = /[a-z]/;
    var numeros = /[0-9]/;
    var caracte = /[$@!]/;
    var cont    = 0;

    var check = ['longitud','mayuscula','minuscula','numero','caracteres'];

    if(val.length>=7){ 
      $("input[type='checkbox'][value='"+check[0]+"']").prop('checked',true);
      cont++;
    }else{
      $("input[type='checkbox'][value='"+check[0]+"']").removeAttr('checked');
    }
    if(mayuscu.test(val)){ 
      $("input[type='checkbox'][value='"+check[1]+"']").prop('checked',true);
      cont++;
    }else{
      $("input[type='checkbox'][value='"+check[1]+"']").removeAttr('checked');
    }
    if(minuscu.test(val)){ 
      $("input[type='checkbox'][value='"+check[2]+"']").prop('checked',true);
      cont++;
    }else{
      $("input[type='checkbox'][value='"+check[2]+"']").removeAttr('checked');
    }
    if(numeros.test(val)){ 
      $("input[type='checkbox'][value='"+check[3]+"']").prop('checked',true);
      cont++;
    }else{
      $("input[type='checkbox'][value='"+check[3]+"']").removeAttr('checked');
    } 
    if(caracte.test(val)){ 
      $("input[type='checkbox'][value='"+check[4]+"']").prop('checked',true);
      cont++;
    }else{
      $("input[type='checkbox'][value='"+check[4]+"']").removeAttr('checked');
    } 
    return cont; 
  }

  events.validation_password = function(){
    var input_pass = DOM.form_chg_passw.find("input#password-new");
    input_pass.on('keyup',function(){
      var val = $(this).val();
      var cont = events.filter_parameter(val);                
      
    });
  };



  events.validation = function(){
    DOM.form_chg_passw.validate({
      success: function(form){
      },
      onSubmit: function(element) {
        $(element).valid();
      },
      submitHandler: function(form) {
        $('#success_change_password').modal('show');
      }
    });

    DOM.form_chg_passw.find("input#password-current").inputmask('Regex', {regex: "[A-Za-z0-9$@!]{1,15}$"}); 
    DOM.form_chg_passw.find("input#password-new").inputmask('Regex', {regex: "[A-Za-z0-9$@!]{1,15}$"}); 
    DOM.form_chg_passw.find("input#password-confirm").inputmask('Regex', {regex: "[A-Za-z0-9$@!]{1,15}$"}); 
    
 

    $.validator.addMethod("method_adicional_criterios", function(value, element) {
      var cont = events.filter_parameter(value);
      var retorno;
      if(cont==5){
        retorno = true;
      }else{
        retorno = false;
      }
      return retorno;
    }, "Revisa las características para la contraseña");
    DOM.form_chg_passw.find("input#password-new").rules("add", {
      method_adicional_criterios : true
    }); 


    DOM.form_chg_passw.find("input#password-confirm").rules("add", {
      equalTo: "#password-new",
      messages :{
        equalTo: "La confirmación de su contraseña no es válida"
      }
    });    
  };  
    
 


  return {
    start: init
  };

})();

$(function(){
  change_password.start();
});

