
/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

 
var mod_comunicados = (function(){
    // Elementos del DOM
    var DOM = {
        form_comunicados     :    $('#form-comunicados') 
      };

    var init = function(){
        events.load_page_function();
        events.load_autocomplete_aerolinea();
    };

    events={};
    events.load_table_comunicados = function(){
        $(".content-comunicados").show();
        var dataHtml = events.renderJson();
        $('#com_table_data tbody').html(dataHtml);
        events.fnPintarTablaComunicados('#com_table_data');

        if ($('#aerolineas').val() == "") {
            $(".content-comunicados").hide();
        }
    }

    events.renderJson = function (){
        var htmlAppenTable = "";
        var jsondata = data_comunicados;
    
        for(var i=0; i<jsondata.length; i++){
            htmlAppenTable += "<tr>"
            htmlAppenTable += "<td class='table-agil---center'>" + jsondata[i].fecha_publicacion + "</td>";
            htmlAppenTable += "<td>" + jsondata[i].descripcion + "</td>";
            htmlAppenTable += "<td>";
            htmlAppenTable += " <a href='"+jsondata[i].archivo+"' target='_blank' class='content-comunicados_file-data--colorred'><span class='glyphicon glyphicon-file'></span></a>";
            htmlAppenTable += "</td>";
            htmlAppenTable += "</tr>";
        }
        return htmlAppenTable;
    }

    events.load_autocomplete_aerolinea = function(){
        var dataList = document.querySelector('#json-aerolineas');
        var jsonOptions = list_aerolineas;

        for(var i=0; i< jsonOptions.length; i++){
            var option = document.createElement('option');
            option.value = jsonOptions[i];
            dataList.appendChild(option);
        }
    } 
    
    events.fnPintarTablaComunicados =function(idTabla){
        var tablaDatos = $(idTabla).DataTable(
            {
                "lengthChange": false,
                "pageLength": 10,
                "paging": true,
                "ordering": false,
                "info": false,
                "searching": false,
                /*"compact": false,*/
                "order": [],
                /*"scrollX": true,*/
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                "language": {
                    "lengthMenu": "Agrupar _MENU_ filas por página",
                    "zeroRecords": "No existen registros",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles.",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Filtrar:",
                    "paginate": {
                        "first": "<",
                        "last": ">",
                        "next": "»",
                        "previous": "«"
                    }
                }
                
            }
        );
        $(idTabla + ' tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                tablaDatos.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
    }

    events.load_page_function = function(){
        $(".content-comunicados").hide();
        if ($('#aerolineas').val() == "") {
            $(".content-comunicados").hide();
        }
    }
    
    return {
        start: init
    };

})();
  
$(function(){
    mod_comunicados.start();
});


$('#aerolineas').change(function(event){
    $(".content-comunicados").hide();
    fn_destroyTable();
    events.load_table_comunicados();
});

$('#aerolineas').keydown(function(event) {
    if(event.keyCode === 13){
        debugger;
        event.preventDefault();
        list_aerolineas.forEach(function(item) {
            if ($('#aerolineas').val() == item) {
                $(".content-comunicados").show();
                fn_destroyTable();
                events.load_table_comunicados();
            }
        });
    }
});


function fn_destroyTable(){
    var tablaDatos = $("#com_table_data tbody");
    $("#com_table_data").DataTable().destroy();
    tablaDatos.empty(); 
}


// data
var data_comunicados = [
    {
        fecha_publicacion :  "01/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "02/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "03/09/2019",
        descripcion: "Comunicado sobre cambio de nombre Lorem Ipsum es simplemente el texto de relleno",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "04/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "05/09/2019",
        descripcion: "Comunicado sobre cambio de nombre Lorem Ipsum es simplemente el texto de relleno",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "06/09/2019",
        descripcion: "Comunicado sobre cambio de nombre Lorem Ipsum es simplemente el texto de relleno",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "07/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "08/09/2019",
        descripcion: "Comunicado sobre cambio de nombre Lorem Ipsum es simplemente el texto de relleno",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "09/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },{
        fecha_publicacion :  "10/09/2019",
        descripcion: "Comunicado sobre cambio de nombre Lorem Ipsum es simplemente el texto de relleno",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "11/09/2019",
        descripcion: "Comunicado sobre cambio de nombre Lorem Ipsum es simplemente el texto de relleno",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "12/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "13/09/2019",
        descripcion: "Comunicado sobre cambio de nombre Lorem Ipsum es simplemente el texto de relleno",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "14/09/2019",
        descripcion: "Comunicado sobre cambio de nombre Lorem Ipsum es simplemente el texto de relleno",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "15/09/2019",
        descripcion: "Comunicado sobre cambio de nombre Lorem Ipsum es simplemente el texto de relleno",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "16/09/2019",
        descripcion: "Comunicado sobre cambio de nombre Lorem Ipsum es simplemente el texto de relleno",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "17/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "18/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "19/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "20/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "21/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "22/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "23/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "24/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "25/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "26/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    },
    {
        fecha_publicacion :  "27/09/2019",
        descripcion: "Comunicado sobre cambio de nombre",
        archivo: "./pdf/Tarifarios.pdf"
    }
];
var list_aerolineas = [
    "Aerolineas Argentinas",
    "Aeromexico",
    "Air Canada",
    "Air Europa",
    "Air France",
    "AirlineMix",
    "Alitalia",
    "American Airlines",
    "Avianca",
    "British Airways",
    "Copa Airelines",
    "Delta Air Lines",
    "Iberia Airlines",
    "KLM",
    "Lufthansa",
    "United Airlines"
 ];