/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

var en_contacto = (function(){
  var request = new XMLHttpRequest();
  // Elementos del DOM
  var DOM = {
    checkboxEmpresa :                $("input[name='empresa']"),
    checkboxTipoBusqueda :           $("input[name='radioBusqueda']"),
    seccionTitulo_DM:                $('.en-contacto-principal-opaco__titulo_DM'),
    seccionDireccion_DM:             $('.en-contacto-principal-opaco__direccion_DM'),
    seccionTitulo_IA:                $('.en-contacto-principal-opaco__titulo_IA'), 
    seccionDireccion_IA:             $('.en-contacto-principal-opaco__direccion_IA'),
    seccionImagenPrincipal:          $('.en-contacto-principal'),
    seccionTarjetaEnContacto:        $('.grupo-tarjeta-encontanto'),
    seccionTarjetaEnContacto_IA:     $('.grupo-tarjeta-encontanto-IA'),
    seccionBusquedaAvanzada:         $('.en-contacto-busqueda-avanzada'),
    seccionSelectorCargo:            $('.selector-cargo'),
    seccionNombreCargo:              $('.textoCargo'),
    seccionNombre:                   $('.textoNombre'),
    droplist_cargo:                  $('#droplist_cargo'),
    input_list_encontactoCargo:      $('#list-encontactoCargo'),
    input_list_encontactoNombre:     $('#list-encontactoNombre'),
    dataListInputCargo:              document.querySelector('#json-aerolineas'),
    dataListInputNombre:             document.querySelector('#json-aerolineas'),
    inputCargo:                      document.querySelector('#list-encontactoCargo'),
    inputNombre:                     document.querySelector('#list-encontactoNombre') ,
    seccionDatosContacto:            $(".datos-en-contacto"),
    tablaDatosContacto:              $(".tabla-datos-en-contacto")
  };

  var init = function(){
    events.choose_empresa();
    events.choose_tipo_busqueda();
    events.init_dropdown();
    events.procesajson();
    events.activa_imagen_persona();
    events.activa_imagen_con_Cargo();
    events.activa_imagen_con_Nombre();
  };
  
  var events = {};

  events.activa_imagen_con_Cargo=function(){
    DOM.input_list_encontactoCargo.keydown(function(event) {
      if (event.keyCode === 13) {    
        event.preventDefault();
        var jsonOptions = JSON.parse(request.responseText);
        // Iterando sobre el arreglo JSON
        jsonOptions.forEach(function(item) {
          // Preguntamos si tenemos un valor que estamos escribiendo y que esta en la lista
          if (DOM.input_list_encontactoCargo.val() == item) {
            DOM.seccionDatosContacto.show();
            DOM.tablaDatosContacto.show();
          }
        });    
      }
      if (DOM.input_list_encontactoCargo.val() == "") {
        DOM.seccionDatosContacto.hide();
        DOM.tablaDatosContacto.hide();
      }
    });
  }

  events.activa_imagen_con_Nombre=function(){
    DOM.input_list_encontactoNombre.keydown(function(event) {
      if (event.keyCode === 13) {    
        event.preventDefault();
        var jsonOptions = JSON.parse(request.responseText);
        // Iterando sobre el arreglo JSON
        jsonOptions.forEach(function(item) {
          // Preguntamos si tenemos un valor que estamos escribiendo y que esta en la lista
          if (DOM.input_list_encontactoNombre.val() == item) {
            DOM.seccionDatosContacto.show();
            DOM.tablaDatosContacto.show();
          }
        });
    
      }
      if (DOM.input_list_encontactoNombre.val() == "") {
        DOM.seccionDatosContacto.hide();
        DOM.tablaDatosContacto.hide();

      }
    });
  }

  events.activa_imagen_persona=function(){
      DOM.input_list_encontactoCargo.change(function(){
        DOM.seccionDatosContacto.show();
        DOM.tablaDatosContacto.show();
        if (DOM.input_list_encontactoCargo.val() == "") {
          DOM.seccionDatosContacto.hide();   
          DOM.tablaDatosContacto.hide();         
        }
    });
    DOM.input_list_encontactoNombre.change(function(){
      DOM.seccionDatosContacto.show();
      DOM.tablaDatosContacto.show();
      if (DOM.input_list_encontactoNombre.val() == "") {
        DOM.seccionDatosContacto.hide();         
        DOM.tablaDatosContacto.hide();   
      }
  });
  }

  events.procesajson=function(){
    request.open('GET', './data/nombres_EnContacto.json', true);
    request.send();    
    request.onreadystatechange = function(response) {
      if (request.readyState === 4) {
        if (request.status === 200) {
          // Parseando JSON
          var jsonOptions = JSON.parse(request.responseText);
          // Iterando sobre el arreglo JSON
          jsonOptions.forEach(function(item) {
            // Creando un option por cada país.
            var option = document.createElement('option');
            option.value = item;
            DOM.dataListInputCargo.appendChild(option);
            DOM.dataListInputNombre.appendChild(option);
          });
          DOM.inputCargo.placeholder = "Escribe el nombre del asesor";
          DOM.inputNombre.placeholder = "Escribe el nombre del asesor";
        } else {
          // Error!
          DOM.inputCargo.placeholder = "Error al cargar lista de personas ;^(";
          DOM.inputNombre.placeholder = "Error al cargar lista de personas ;^(";
        }
      }
    };
    DOM.inputCargo.placeholder ="Escribe el nombre del asesor";
    DOM.inputNombre.placeholder = "Escribe el nombre del asesor";
  }

  events.choose_empresa=function(){
    DOM.checkboxEmpresa.on('change',function(e){
      var val = $(this).val();
      DOM.seccionImagenPrincipal.show();
      DOM.seccionBusquedaAvanzada.show();
      if(val=="DM"){
        DOM.seccionTarjetaEnContacto.show();
        DOM.seccionTarjetaEnContacto_IA.hide();
        DOM.seccionTitulo_DM.show();
        DOM.seccionDireccion_DM.show();
        DOM.seccionTitulo_IA.hide();
        DOM.seccionDireccion_IA.hide();
      }else{      
        DOM.seccionTarjetaEnContacto_IA.show();
        DOM.seccionTarjetaEnContacto.hide();
        DOM.seccionTitulo_IA.show();
        DOM.seccionDireccion_IA.show();
        DOM.seccionTitulo_DM.hide();
        DOM.seccionDireccion_DM.hide();

        DOM.seccionNombre.hide();
        DOM.seccionSelectorCargo.hide();
        DOM.seccionNombreCargo.hide();
        DOM.seccionDatosContacto.hide();
        DOM.tablaDatosContacto.hide();
      }
    })
  }

  events.choose_tipo_busqueda=function(){
    DOM.checkboxTipoBusqueda.on('change',function(e){
      var val = $(this).val();
      if(val=="Cargo"){          
          DOM.seccionSelectorCargo.show();
          DOM.seccionNombre.hide();
          DOM.droplist_cargo.ddslick('select', {index: 0 });
      }else{
          DOM.seccionNombre.show();
          DOM.seccionSelectorCargo.hide();
          DOM.seccionNombreCargo.hide();
          DOM.seccionDatosContacto.hide();
          DOM.tablaDatosContacto.hide();
 
      }
    })
  }

  events.init_dropdown = function(){
    DOM.droplist_cargo.ddslick({
      onSelected: function (data) {
        var value = data.selectedData.value;
        if(value!=0){
            DOM.seccionNombreCargo.show();
        }     
      }
    });
  }

    
  return {
    start: init
  };

})();

$(function(){
  en_contacto.start();
});
