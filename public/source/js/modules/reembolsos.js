/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

var reembolsos = (function(){

    // Elementos del DOM
    var DOM = {
      form_reembolsos: $('#form-reembolsos'),
      estados_reembolsos: $('#estados_reembolsos'),
      desde_datepicker: $('#periodo_ini'),
      hasta_datepicker: $('#periodo_fin')
    };
  
    var init = function(){
      events.validation_form();
      events.init_calendar();
      events.init_dropdown();
      events.load_table();
      events.val_orden_dates();
    };  
  
    var events = {};

    events.invertir_fecha =  function(fecha){
      var arrfecha = fecha.split("/");
      return arrfecha[2]+'/'+arrfecha[1]+'/'+arrfecha[0];
    }

    events.val_orden_dates = function(){
      var btn = DOM.form_reembolsos.find('button.Button[type="submit"]');
      btn.on('click',function(e){
        e.preventDefault();
        var periodo_ini = DOM.desde_datepicker;
        var periodo_fin = DOM.hasta_datepicker;
        
        if(periodo_ini.val()!=='' && periodo_fin.val() !== ''){
          var f_ini = periodo_ini.val();
          var f_fin = periodo_fin.val();
          f_ini = events.invertir_fecha(f_ini);
          f_fin = events.invertir_fecha(f_fin);          
          var per_ini = new Date(f_ini).getTime();
          var per_fin = new Date(f_fin).getTime();
          if(per_ini > per_fin){
            alert("La Fecha Desde debe ser menor a la Fecha Hasta");
            return false;
          }              
          //$(this).parent().parent().hide();
        }else{
          if(periodo_ini.val() == ''){
            periodo_ini.addClass("error");
            periodo_ini.focus();
          }
          if(periodo_fin.val() == ''){
            periodo_fin.addClass("error");
            periodo_fin.focus();
          }
        }
  
      })
    }

    events.load_table = function(){
      var dataHtml = events.renderJson();
      $('#reembolsos__table tbody').html(dataHtml);
      events.fnPintarTablaComunicados('#reembolsos__table');
    }  
    
    events.generic_data = function(){
      
      var data_comunicados  = [];
      var n_boleto          = ['4745823569','3157178673'];     
      var name_pax          = ['CARDENAS/ROSA','FERNANDO/JAIRO'];
      var line_air          = ['H2','H1'];
      var method_pay        = ['CASH','CREDITO'];
      var method_pay        = ['CASH','CREDITO'];           
      var states            = ['FINALIZADO','FINALIZADO','FINALIZADO'];                       
      for (let index = 0; index < 100; index++) {
        const row =     {
          n_boleto    : n_boleto[Math.floor(Math.random() * 2)],
          name_pax    : name_pax[Math.floor(Math.random() * 2)],
          line_air    : line_air[Math.floor(Math.random() * 2)],
          method_pay  : method_pay[Math.floor(Math.random() * 2)],
          states      : states[Math.floor(Math.random() * 3)]                    
        }
        data_comunicados.push(row);
        //range(10)
      }

      return data_comunicados;
    }
    events.renderJson = function (){
      var htmlAppenTable = "";
      var jsondata = events.generic_data();
      for(var i=0; i<jsondata.length; i++){
          htmlAppenTable += "<tr>"
          htmlAppenTable += "<td class='table-agil---center'>" + jsondata[i].n_boleto + "</td>";
          htmlAppenTable += "<td class='table-agil---center'>" + jsondata[i].name_pax + "</td>";
          htmlAppenTable += "<td class='table-agil---center'>" + jsondata[i].line_air + "</td>";
          htmlAppenTable += "<td class='table-agil---center'>" + jsondata[i].method_pay + "</td>";
          htmlAppenTable += "<td class='table-agil---center'>" + jsondata[i].states + "</td>";          
          htmlAppenTable += "<td class='table-agil---center'><a href='#' data-toggle='modal' data-target='#modal_requisito_reembolsos'><i class='icon-list'></i></a></td>"; 
          htmlAppenTable += "</tr>";
      }
      return htmlAppenTable;
    }

    events.fnPintarTablaComunicados =function(idTabla){
      var tablaDatos = $(idTabla).DataTable(
          {
              "lengthChange": false,
              "pageLength": 10,
              "paging": true,
              "ordering": false,
              "info": false,
              "searching": false,
              /*"compact": false,*/
              "order": [],
              /*"scrollX": true,*/
              "columnDefs": [{
                  "targets": 'no-sort',
                  "orderable": false,
              }],
              "language": {
                  "lengthMenu": "Agrupar _MENU_ filas por página",
                  "zeroRecords": "No existen registros",
                  "info": "Página _PAGE_ de _PAGES_",
                  "infoEmpty": "No hay registros disponibles.",
                  "infoFiltered": "(filtered from _MAX_ total records)",
                  "search": "Filtrar:",
                  "paginate": {
                      "first": "<",
                      "last": ">",
                      "next": "»",
                      "previous": "«"
                  }
              }
              
          }
      ); 
    }   

    events.get_date_format =  function(days_last){
      var date      = new Date();
      var year      = date.getFullYear();
      var month     = date.getMonth();
      month         = month+1;
      month         = (month<10) ? '0'+month : month;
      var day       = date.getDate();
      day           = (day<10) ? '0'+day : day;
      var hoy       = day+'-'+month+'-'+year;

      var date      = new Date();
      var startDate = date.setDate(date.getDate() - days_last);
      startDate = new Date(startDate);
      month         = startDate.getMonth();
      month         = month+1;
      month         = (month<10) ? '0'+month : month;
      day           = startDate.getDate();
      day           = (day<10) ? '0'+day : day;
      var startDate = day+'-'+month+'-'+startDate.getFullYear();
      return {'hoy':hoy,'startDate':startDate}
  
    };    

    events.init_calendar = function(){   
      var days_last_ini   =  DOM.desde_datepicker.data("days-last");   
      var rptas           = events.get_date_format(days_last_ini);
      var startDate       = rptas.startDate;
      var hoy             = rptas.hoy;      
      DOM.desde_datepicker.datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        language: 'es',
        startDate: startDate,
        endDate: hoy
      });

      var days_last_ini  = DOM.hasta_datepicker.data("days-last");   
      var rptas           = events.get_date_format(days_last_ini);
      var startDate       = rptas.startDate;
      var hoy             = rptas.hoy;  
      DOM.hasta_datepicker.datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        language: 'es',
        startDate: startDate,
        endDate: hoy
      });      
    };

    

    events.init_dropdown = function(){
      DOM.estados_reembolsos.ddslick(); 
    }
    events.validation_form = function(){
      DOM.form_reembolsos.validate({
        success: function(form){ 
        },
        onSubmit: function(element) {
          $(element).valid();
        },
        submitHandler: function(form) {
          
        }
      });
      DOM.form_reembolsos.find("input#periodo_ini").inputmask({"mask": "99/99/9999"});
      DOM.form_reembolsos.find("input#periodo_fin").inputmask({"mask": "99/99/9999"});  
    };  
      
    return {
      start: init
    };
  
  })();
  
  $(function(){
    reembolsos.start();
  });

