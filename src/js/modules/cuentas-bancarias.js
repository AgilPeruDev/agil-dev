/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

var cuentas_bancarias = (function(){
    var DOM = {
        tipo_cuenta :                $("input[name='tipo_cuenta']"),
        panel_showcuentas_bancarias: $(".panel-showcuentas_bancarias"),
        form_interagencias :         $(".result-interagencias"),
        form_dest_mund :             $(".result-dest-mund")
    }
    var init = function(){
        events.choose_tipo_cuenta();
        events.fn_default_load();
        events.fn_acordeonContenedor();
    };

    var events = {};

    events.fn_default_load = function(){
        DOM.panel_showcuentas_bancarias.hide();
        DOM.form_dest_mund.hide();
        DOM.form_interagencias.hide();
    }

    events.choose_tipo_cuenta = function(){
        DOM.tipo_cuenta.on('change',function(e){
            var val = $(this).val();
            DOM.panel_showcuentas_bancarias.show();
          /* val es el ripo de listado. deonde:
           *1 = ineteragencias
           *2 = destinos mundiales
           */
          if(val=="1"){
              /* Llamar al servicio y retornar el json
               *Parametro que sera utilizado en la funcion
               */
                var json = {};
                events.list_cuentas_ineteragencias(val, json);            
          }else{   
               /* Llamar al servicio y retornar el json
               *Parametro que sera utilizado en la funcion
               */
                var json = {};
                events.list_cuentas_destinosmundiales(val, json); 
          }
        })
    }

    events.list_cuentas_destinosmundiales = function(empresa, data){
        DOM.form_interagencias.hide();
        DOM.form_dest_mund.show();
         /* Obtenermos Json */
        /* Obtenermos Json */
        /*Cargar valor jsonData con el parametro data */
        var jsonData = Array_cuentas_destinos_mundiales; /*data */
        /*Filtramos solo bancos Unicos
         *Mostramos los Bancos*/
        var var_UniqueBank = events.fn_filterUniqueBank(jsonData);
        var image = "", divImage="", arrayBanco=[], contentBank="";
        for(var i=0; i<var_UniqueBank.length; i++){
            switch (var_UniqueBank[i].banco) {
                case "BCP":
                    image = "./img/bcp.png";
                    for(var x =0; x < jsonData.length; x++){
                        if(var_UniqueBank[i].banco == jsonData[x].banco){
                            arrayBanco.push(jsonData[x]);
                        }
                    }
                    contentBank = events.fn_createContentBank(empresa, arrayBanco, var_UniqueBank[i].banco);
                  break;
                case "Interbank":
                    image = "./img/ibk.png";
                    for(var x =0; x < jsonData.length; x++){
                        if(var_UniqueBank[i].banco == jsonData[x].banco){
                            arrayBanco.push(jsonData[x]);
                        }
                    }
                    contentBank = events.fn_createContentBank(empresa, arrayBanco, var_UniqueBank[i].banco);
                  break;
                case "BBVA":
                    image = "./img/bbva.png";
                    for(var x =0; x < jsonData.length; x++){
                        if(var_UniqueBank[i].banco == jsonData[x].banco){
                            arrayBanco.push(jsonData[x]);
                        }
                    }
                    contentBank = events.fn_createContentBank(empresa, arrayBanco, var_UniqueBank[i].banco);
                   break;
                case "Scotiabank":
                    image = "./img/sbk.png";
                    for(var x =0; x < jsonData.length; x++){
                        if(var_UniqueBank[i].banco == jsonData[x].banco){
                            arrayBanco.push(jsonData[x]);
                        }
                    }
                    contentBank = events.fn_createContentBank(empresa, arrayBanco, var_UniqueBank[i].banco);
            }
            divImage = '<img src='+ image +'></img>';
            $('.result-dest-mund .bank'+ (i+1) ).show();
            $('.result-dest-mund .bank'+ (i+1) +' .seccion-banco .img-logo').html(divImage);
            $('.result-dest-mund .bank'+ (i+1) +' .seccion-banco-result').html(contentBank);
            //Limpiamos el array
            arrayBanco = [];
        }
    }  

    events.list_cuentas_ineteragencias = function(empresa, data){
        DOM.form_dest_mund.hide();
        DOM.form_interagencias.show();
        /* Obtenermos Json */
        /*Cargar valor jsonData con el parametro data */
        var jsonData = Array_cuentas_interagencias; /*data */
        /*Filtramos solo bancos Unicos
         *Mostramos los Bancos*/
        var var_UniqueBank = events.fn_filterUniqueBank(jsonData);
        var image = "", divImage="", arrayBanco=[], contentBank="";
        for(var i=0; i<var_UniqueBank.length; i++){
            switch (var_UniqueBank[i].banco) {
                case "BCP":
                    image = "./img/bcp.png";
                    for(var x =0; x < jsonData.length; x++){
                        if(var_UniqueBank[i].banco == jsonData[x].banco){
                            arrayBanco.push(jsonData[x]);
                        }
                    }
                    contentBank = events.fn_createContentBank(empresa, arrayBanco, var_UniqueBank[i].banco);
                  break;
                case "Interbank":
                    image = "./img/ibk.png";
                    for(var x =0; x < jsonData.length; x++){
                        if(var_UniqueBank[i].banco == jsonData[x].banco){
                            arrayBanco.push(jsonData[x]);
                        }
                    }
                    contentBank = events.fn_createContentBank(empresa, arrayBanco, var_UniqueBank[i].banco);
                  break;
                case "BBVA":
                    image = "./img/bbva.png";
                    for(var x =0; x < jsonData.length; x++){
                        if(var_UniqueBank[i].banco == jsonData[x].banco){
                            arrayBanco.push(jsonData[x]);
                        }
                    }
                    contentBank = events.fn_createContentBank(empresa, arrayBanco, var_UniqueBank[i].banco);
                   break;
                case "Scotiabank":
                    image = "./img/sbk.png";
                    for(var x =0; x < jsonData.length; x++){
                        if(var_UniqueBank[i].banco == jsonData[x].banco){
                            arrayBanco.push(jsonData[x]);
                        }
                    }
                    contentBank = events.fn_createContentBank(empresa, arrayBanco, var_UniqueBank[i].banco);
            }
            divImage = '<img src='+ image +'></img>';
            $('.result-interagencias .bank'+ (i+1) ).show();
            $('.result-interagencias .bank'+ (i+1) +' .seccion-banco .img-logo').html(divImage);
            $('.result-interagencias .bank'+ (i+1) +' .seccion-banco-result').html(contentBank);
            //Limpiamos el array
            arrayBanco = [];
        }
    }
    events.fn_createContentBank = function(empresa, jsonBank, banco){
        var htmlBank = "";
        htmlBank += '<table>';
        htmlBank +=     events.fn_createHeader(empresa, banco);
        htmlBank +=     events.fn_createBody(empresa, jsonBank, banco);
        htmlBank += '</table>';
        return htmlBank;

    }
    events.fn_createHeader = function(empresa, banco){
        var htmlHead = "";
        if(empresa == '1'){
            htmlHead +='<thead>';
            htmlHead +='<tr>';
            if ('BCP' == banco){
                htmlHead +='<th>Ciudad</th>';
            }
            htmlHead +='<th>Moneda</th>';
            if ('BCP' != banco){
                htmlHead +='<th>Cuenta Corriente</th>';
            }else{
                htmlHead +='<th>Cuentas Bancarias</th>';
            }
            htmlHead +='</tr>';
            htmlHead +='</thead>';
    
        }else{
            htmlHead +='<thead>';
            htmlHead +='<tr>';
            htmlHead +='<th>Moneda</th>';
            htmlHead +='<th>Cuenta Corriente</th>';
            htmlHead +='<th>Código Interbancario</th>';
            htmlHead +='</tr>';
            htmlHead +='</thead>';
        }


        return htmlHead;
    }
    events.fn_createBody = function(empresa, jsonBank, banco){
        var htmlBody = "";
        htmlBody +='<tbody>';
        if(empresa == '1'){
            for(var i=0; i < jsonBank.length; i++){
                htmlBody +='<tr>';
                if ('BCP' == banco){
                    htmlBody +='<td>'+jsonBank[i].ciudad+'</td>';
                }
                htmlBody +='<td>'+jsonBank[i].moneda+'</td>';
                htmlBody +='<td>'+jsonBank[i].cuenta_corriente+'</td>';
                htmlBody +='</tr>';
            }
        }else{
            for(var i=0; i < jsonBank.length; i++){
                htmlBody +='<tr>';
                htmlBody +='<td>'+jsonBank[i].moneda+'</td>';
                htmlBody +='<td>'+jsonBank[i].cuenta_corriente+'</td>';
                htmlBody +='<td>'+jsonBank[i].codigo_interbancario+'</td>';
                htmlBody +='</tr>';
            }
        }
        
        htmlBody +='</tbody>';
        return htmlBody;
    }
    events.fn_filterUniqueBank = function(p_cuentas){
        const list_banks_filter = [];
        const map = new Map();
        for (const item of p_cuentas) {
            if(!map.has(item.banco)){
                map.set(item.banco, true); 
                list_banks_filter.push({
                    banco: item.banco
                });
            }
        }
        return list_banks_filter;
    }
    events.fn_acordeonContenedor = function(){
        /*  INTERAGENCIAS */
        $(".result-interagencias .bank1 .seccion-banco").click(function(){
            events.fn_show_hide_inter(1);
        });
        $(".result-interagencias .bank2 .seccion-banco").click(function(){
            events.fn_show_hide_inter(2);
        });
        $(".result-interagencias .bank3 .seccion-banco").click(function(){
            events.fn_show_hide_inter(3);
        });
        $(".result-interagencias .bank4 .seccion-banco").click(function(){
            events.fn_show_hide_inter(4);
        });
        $(".result-interagencias .bank5 .seccion-banco").click(function(){
            events.fn_show_hide_inter(5);
        });

          /*  DESTINOS MUNDIALES */
          $(".result-dest-mund .bank1 .seccion-banco").click(function(){
            events.fn_show_hide_dm(1);
            /*$(".result-dest-mund .banco-result .down-open").removeClass('open-tab');
            $(".result-dest-mund .bank1 .down-open").addClass('open-tab');*/
        });
        $(".result-dest-mund .bank2 .seccion-banco").click(function(){
            events.fn_show_hide_dm(2);
            /*$(".result-dest-mund .banco-result .down-open").removeClass('open-tab');
            $(".result-dest-mund .bank2 .down-open").addClass('open-tab');*/
        });
        $(".result-dest-mund .bank3 .seccion-banco").click(function(){
            events.fn_show_hide_dm(3);
            /*$(".result-dest-mund .banco-result .down-open").removeClass('open-tab');
            $(".result-dest-mund .bank3 .down-open").addClass('open-tab');*/
        });
        $(".result-dest-mund .bank4 .seccion-banco").click(function(){
            events.fn_show_hide_dm(4);
            /*$(".result-dest-mund .banco-result .down-open").removeClass('open-tab');
            $(".result-dest-mund .bank4 .down-open").addClass('open-tab');*/
        });
        $(".result-dest-mund .bank5 .seccion-banco").click(function(){
            events.fn_show_hide_dm(5);
            /*$(".result-dest-mund .banco-result .down-open").removeClass('open-tab');
            $(".result-dest-mund .bank4 .down-open").addClass('open-tab');*/
        });
        
    }
    events.fn_show_hide_inter = function(p_seccion){       
        if($('.result-interagencias .bank'+p_seccion + ' .seccion-banco-result').css("display") == "block"){
            $('.result-interagencias .bank'+p_seccion + ' .seccion-banco-result').hide();
            $('.result-interagencias .banco-result .down-open').removeClass('open-tab');
        }else{
            $('.result-interagencias .seccion-banco-result').hide();
            $('.result-interagencias .bank'+p_seccion + ' .seccion-banco-result').show();

            $('.result-interagencias .banco-result .down-open').removeClass('open-tab');
            $('.result-interagencias .bank'+ p_seccion+' .down-open').addClass('open-tab');
        }

    }

    events.fn_show_hide_dm = function(p_seccion){
        /*$('.result-dest-mund .seccion-banco-result').hide();
        $('.result-dest-mund .bank'+p_seccion + ' .seccion-banco-result').show();*/
        if($('.result-dest-mund .bank'+p_seccion + ' .seccion-banco-result').css("display") == "block"){
            $('.result-dest-mund .bank'+p_seccion + ' .seccion-banco-result').hide();
            $('.result-dest-mund .banco-result .down-open').removeClass('open-tab');
        }else{
            $('.result-dest-mund .seccion-banco-result').hide();
            $('.result-dest-mund .bank'+p_seccion + ' .seccion-banco-result').show();

            $('.result-dest-mund .banco-result .down-open').removeClass('open-tab');
            $('.result-dest-mund .bank'+ p_seccion+' .down-open').addClass('open-tab');
        }
    }
    return {
        start: init
    };

})();

$(function(){
    cuentas_bancarias.start();
});


var Array_cuentas_destinos_mundiales = [
	{
		"banco": "BCP",
		"ciudad": "Lima",
		"moneda": "Soles",
        "cuenta_corriente": "191-2260456242435-49",
        "codigo_interbancario": "191-226-045624243524-49",
        "descripcion": "Lorem Ipsum is simply dummy text of printing."
    },
	{
		"banco": "BBVA",
		"ciudad": "Lima",
		"moneda": "Soles",
        "cuenta_corriente": "191-2260456242435-49",
        "codigo_interbancario": "191-226-045624243524-49",
        "descripcion": "Lorem Ipsum is simply dummy text of printing."
    }
 ];

 var Array_cuentas_interagencias = [
	{
		"banco": "BCP",
		"ciudad": "Lima",
		"moneda": "Soles",
        "cuenta_corriente": "191-2260456242435-49",
        "codigo_interbancario": "191-226-045624243524-49",
    },
    {
		"banco": "BCP",
		"ciudad": "Arequipa",
		"moneda": "Soles",
        "cuenta_corriente": "191-2260456242435-49",
        "codigo_interbancario": "191-226-045624243524-49",
    },
    {
		"banco": "BCP",
		"ciudad": "Tacna",
		"moneda": "Soles",
        "cuenta_corriente": "191-2260456242435-49",
        "codigo_interbancario": "191-226-045624243524-49",
    },
    {
		"banco": "BCP",
		"ciudad": "Cuzco",
		"moneda": "Soles",
        "cuenta_corriente": "191-2260456242435-49",
        "codigo_interbancario": "191-226-045624243524-49",
    },
	{
		"banco": "Interbank",
		"ciudad": "Lima",
		"moneda": "Dólares",
        "cuenta_corriente": "191-2260456242435-49",
        "codigo_interbancario": "191-226-045624243524-49",
    },
	{
		"banco": "BBVA",
		"ciudad": "Lima",
		"moneda": "Soles",
        "cuenta_corriente": "191-2260456242435-49",
        "codigo_interbancario": "191-226-045624243524-49",
    },
	{
		"banco": "Scotiabank",
		"ciudad": "Lima",
		"moneda": "Soles",
        "cuenta_corriente": "191-2260456242435-49",
        "codigo_interbancario": "191-226-045624243524-49",
    },
    {
		"banco": "Interbank",
		"ciudad": "Lima",
		"moneda": "Dólares",
        "cuenta_corriente": "191-2260456242435-49",
        "codigo_interbancario": "191-226-045624243524-49",
    },
	{
		"banco": "BBVA",
		"ciudad": "Lima",
		"moneda": "Soles",
        "cuenta_corriente": "191-2260456242435-49",
        "codigo_interbancario": "191-226-045624243524-49",
    }
 ];
