function fnPintarTabla(idTabla ) {
    debugger;
    var tablaDatos = $(idTabla).DataTable(
        {
            "lengthChange": false,
            "pageLength": 10,
            "lengthMenu": [10, 20, 30, 40],
            "paging": true,
            "ordering": false,
            "info": false,
            "searching": false,
            "compact": true,
            "order": [],
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false,
            }],
            "language": {
                "lengthMenu": "Agrupar _MENU_ filas por página",
                "zeroRecords": "No existen registros",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles.",
                "infoFiltered": "(filtered from _MAX_ total records)",
                "search": "Filtrar:",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": ">>",
                    "previous": "<<"
                }
            }
           
        }
    );
    
    tablaDatos.on('order.dt search.dt', function () {
        tablaDatos.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
  
    $(idTabla + ' tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            tablaDatos.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });
}

