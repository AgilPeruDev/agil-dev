/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */
//$('#success_forget_password1').modal('show');

var register = (function(){

    // Elementos del DOM
    var DOM = {
      form_forget_password: $('#form-datos-forget'),
      text_email: $('#input_email'),
      form_mensaje_forget:$('.form-mensaje-forget')

    };
  
    var init = function(){
      events.validation_register();
      events.validaInput();
    };  
  
    var events = {};

    events.validation_register = function(){
      DOM.form_forget_password.validate({
        success: function(form){ 
        },
        onSubmit: function(element) {
          $(element).valid();
        },
        submitHandler: function(form) {
            if (DOM.text_email.val()=='roddy.rios@tiexpertia.com'){
              DOM.text_email.val('');
              DOM.form_mensaje_forget.hide();
              $('#success_forget_password1').modal('show');
            }else
              if (DOM.text_email.val()=='sarah.pacheco@expertiatravel.com'){
                DOM.text_email.val('');
                DOM.form_mensaje_forget.hide();
                $('#success_forget_password2').modal('show');
              }else
                if(DOM.text_email.val()=='david.argume@tiexpertia.com'){
                  DOM.text_email.val('');
                  DOM.form_mensaje_forget.hide();
                  $('#success_forget_password3').modal('show');
                }else
                  if(DOM.text_email.val()=='ronald.tenazoa@tiexpertia.com'){
                    DOM.text_email.val('');
                    DOM.form_mensaje_forget.hide();
                    $('#success_forget_password4').modal('show');
                      
                  }else{
                    DOM.form_mensaje_forget.show();
                  }                             
        }
      });
 
      $.validator.addMethod("valida_email", function(value, element) {
        DOM.form_mensaje_forget.hide();
        return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(value);        
      }, "Ingresa un Email válido");

      DOM.text_email.rules("add", {
        valida_email : true
        
      }); 
      
      if(DOM.text_email.length>0) {
        DOM.text_email.inputmask('Regex', {regex: "[A-Za-z0-9._@-]{1,100}$"});
      }

    };  
    
    events.validaInput=function(){
      DOM.text_email.keydown(function(){
          console.log("entro");
          DOM.form_mensaje_forget.hide();   
    });
  }

    return {
      start: init
    };
  
  })();
  
  $(function(){
    register.start();
  });
  