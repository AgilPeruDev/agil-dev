/*$(document).ready(function(){
    debugger;
    var idMap = 'divMapa';
    var ubicacion = { lat: 43.5293101, lng: -5.6773233 };
    mapa = new google.maps.Map(document.getElementById('divMapa'), {
        center: ubicacion,
        zoom: 5
    });    
});*/



/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

var oficinas = (function(){
    var DOM = {
        oficina_encabezado : $(".oficinas-encabezado"),
        oficina_groups: $(".oficina-groups")
    }
    var init = function(){
        events.fn_loadFormOficinas();
    };

    var events = {};

    events.fn_loadFormOficinas = function(){
        var dataOficinas = Array_Oficinas;
        debugger;
        var htmData = events.fn_createGroups(dataOficinas);
        DOM.oficina_groups.html(htmData);
        events.fn_loadMaps(dataOficinas);
    }
    
    events.fn_loadMaps =function(dataOficinas){
        var ubicacion ={};
        var idMaps = '';
        for(var y=0; y<dataOficinas.length; y++){
            ubicacion = { lat: dataOficinas[y].UTM.latitud, lng: dataOficinas[y].UTM.longitud };
            idMaps = 'idMaps'+y;
            
            mapa = new google.maps.Map(document.getElementById(idMaps), {
                center: ubicacion,
                zoom: 5
            });
        }

        
    }
    events.fn_createGroups = function(dataOficinas){
        var htmlGroup = '',
            idDivGrupo = '',
            idMaps ='';

        for(var i =0; i<dataOficinas.length; i++){
           idDivGrupo = 'oficinas-detalle oficina'+i;
           idMaps = 'idMaps'+i;
           htmlGroup += '<div class="'+idDivGrupo+'">'+
                       '   <div class="form-group">'+
                       '      <div class="row">'+
                       
                       '         <div class="col-sm-6">'+
                       '            <div class="title-sede intelo_b">'+
                       '                <h1>Sede <i>'+dataOficinas[i].oficina+'</i></h1>'+
                       '            </div>'+
                       '            <div class="desc-sede">' +dataOficinas[i].name+'</div>'+
                       '            <div class="telefono-sede">'+
                       '                <label>Teléfono:</label>'+
                       '                <label>'+dataOficinas[i].telefono+'</label>'+
                       '            </div>'+
                       '         </div>'+

                       '         <div class="col-sm-6">'+
                       '            <div class="mapa-sede" id="'+idMaps+'">'+
                       '               <label>Maps</label>'+
                        '           </div>'+
                        '         </div>'+

                        '     </div>'+
                        '   </div>'+
                        '</div>';
        }
        return htmlGroup;
    }

    return {
        start: init
    };

})();

$(function(){
    oficinas.start();
});


var Array_Oficinas = [
	{
		"oficina": "Miraflores",
		"principal": true,
		"name": "Lorem Ipsum is simply dummiy text of the printig 01",
        "telefono": "123-4567",
        "UTM": {
            'latitud' : -12.0431800,
            'longitud' : -77.0282400
        }
    },
	{
		"oficina": "Arequipa",
		"principal": true,
		"name": "Lorem Ipsum is simply dummiy text of the printig 02",
        "telefono": "123-4567",
        "UTM": {
            'latitud' : -12.0411100,
            'longitud' : -77.0282111
        }
    },
    {
		"oficina": "Cuzco",
		"principal": true,
		"name": "Lorem Ipsum is simply dummiy text of the printig 03",
        "telefono": "123-4567",
        "UTM": {
            'latitud' : -12.0411122,
            'longitud' : -77.0282122
        }
    },
    {
		"oficina": "Los Olivos",
		"principal": true,
		"name": "Lorem Ipsum is simply dummiy text of the printig 04",
        "telefono": "123-4567",
        "UTM": {
            'latitud' : -12.0422122,
            'longitud' : -77.0332122
        }
    }
 ];


 
