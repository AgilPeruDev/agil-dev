/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

var reporte_adminitrativo = (function(){

    // Elementos del DOM
    var DOM = {
      form_reporte_admin     :    $('#form-reporte-admin'), 
      periodo :                   $(".perido-rango"),
      form_tabla_comprobantes    :$('.form-tabla-comprobantes'),
      form_tabla_boletos         :$('.form-tabla-boletos'),
      droplist_sub_codigo :      $("#sub_codigo"),
      droplist_forma_pago :      $("#forma_pago"),
    };
  
    var init = function(){
      events.init_calendar();
      events.init_dropdown();
      events.fn_eventClickButton();
    };
  
  
    var events = {};
  
    events.get_date_format =  function(years_last){
      var date      = new Date();
      var year      = date.getFullYear();
      var month     = date.getMonth();
      month         = month+1;
      month         = (month<10) ? '0'+month : month;
      var day       = date.getDate();
      day           = (day<10) ? '0'+day : day;
      var hoy       = day+'-'+month+'-'+year;
      var year_last = date.getFullYear()-years_last;
      var startDate = day+'-'+month+'-'+year_last;
      return {'hoy':hoy,'startDate':startDate}
    };

    events.init_calendar = function(){
      var periodo_ini     = DOM.periodo.find("[id='periodo_ini']");      
      var years_last_ini  = periodo_ini.data("years-last");   
      var rptas           = events.get_date_format(years_last_ini);
      var startDate       = rptas.startDate;
      var hoy             = rptas.hoy;
  
      periodo_ini.datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        language: 'es',
        startDate: startDate,
        endDate: hoy
      }); 
      
    
      var periodo_fin     = DOM.periodo.find("[id='periodo_fin']");   
      var years_last_fin  = periodo_fin.data("years-last");  
      var rptas           = events.get_date_format(years_last_fin);
      var startDate       = rptas.startDate;
      var hoy             = rptas.hoy;    
  
      periodo_fin.datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        language: 'es',
        startDate: startDate,
        endDate: hoy
      });  

    };

    events.init_dropdown = function(){
        DOM.droplist_sub_codigo.ddslick();  
        DOM.droplist_forma_pago.ddslick();
    } 

    events.fn_eventClickButton = function(){
        $("#btnConsulta").click(function(){
            var validator = $( "#nro_comprobante" ).validate();

            var objcounter = $("#counter").val(),
                objintinerario = $("#intinerario").val(),
                objpasajero  = $("#pasajero").val(),
                objline_aerea = $("#line_aerea").val();

            if(objcounter != '' || objintinerario != '' || objpasajero != ''|| objline_aerea != ''){
                events.fn_validate_form_Busqueda_conFecha();
            }else if($("#nro_comprobante").val().length > 0){
                events.fn_validate_form_Busqueda_num_comprobante();
            }else if($("#periodo_ini").val().length > 0 || $("#periodo_fin").val().length > 0){
                $("#nro_comprobante").prop('disabled', true);
                events.fn_validate_form_Busqueda_conFecha();
            }else{
                events.fn_validate_form_Busqueda_num_comprobante();
            }
            if (!$("#form-reporte-admin").valid()) {
                return;
            }else{
                document.location='./result-reporte-administrativo.html';
            }
        });

        //crearte mask  
        var input_periodo_ini = DOM.form_reporte_admin.find('#periodo_ini');
        if(input_periodo_ini.length>0) 
            input_periodo_ini.inputmask('99/99/9999');
        var input_periodo_fin = DOM.form_reporte_admin.find('#periodo_fin');
        if(input_periodo_fin.length>0) 
            input_periodo_fin.inputmask('99/99/9999');
        var input_nro_comprobante = DOM.form_reporte_admin.find('#nro_comprobante');
        if(input_nro_comprobante.length>0) 
            input_nro_comprobante.inputmask('Regex', {regex: "[A-Za-z0-9]{12}$"});
        var input_intinerario = DOM.form_reporte_admin.find('#intinerario');
         if(input_intinerario.length>0) 
            input_intinerario.inputmask('Regex', {regex: "[A-Za-z0-9/]{1,11}$"});
        var input_line_aerea = DOM.form_reporte_admin.find('#line_aerea');
        if(input_line_aerea.length>0) 
            input_line_aerea.inputmask('Regex', {regex: "[A-Za-z0-9/]{2,2}$"});
        var input_pasajero = DOM.form_reporte_admin.find('#pasajero');
        if(input_pasajero.length>0) 
            input_pasajero.inputmask('Regex', {regex: "[A-Za-z ]{1,100}$"});
        var input_counter = DOM.form_reporte_admin.find('#counter');
        if(input_counter.length>0) 
            input_counter.inputmask('Regex', {regex: "[A-Za-z ]{1,100}$"});  

        $("#btnLimpiar").click(function(){
             $("#form-reporte-admin").validate().resetForm();
             $("#form-reporte-admin")[0].reset();
        });
    }

    events.fn_validate_form_Busqueda_conFecha = function(){
        $("#nro_comprobante").validate().resetForm();
        $.validator.setDefaults({
            debug: true,
            success: "valid"
        });
        $("#form-reporte-admin").validate({
            rules: {
                nro_comprobante: {
                    required: false,
                    rangelength: [12, 12]
                },
                periodo_ini: {
                    required: true,
                    rangelength: [10, 10]
                },
                periodo_fin: {
                    required: true,
                    rangelength: [10, 10]
                },
                line_aerea:{
                    required: false,
                    rangelength: [2, 2]
                },
                pasajero:{
                    required:false,
                    minlength :5,
                    maxlength :100
                },
                intinerario:{
                    required:false,
                    rangelength:[1,11]
                },
                counter:{
                    required:false,
                    minlength :5,
                    maxlength :100
                },
            },
            messages: {
                nro_comprobante: {
                    required: 'Ingresar el N°. de comprobante.',
                    rangelength : 'Ingresar 12 caracteres.',
                },
                periodo_ini: {
                    required: 'Ingresar la fecha inicio.',
                    rangelength : 'Formato de fecha invalido.'
                },
                periodo_fin: {
                    required: 'Ingresar la fecha fin.',
                    rangelength : 'Formato de fecha invalido.'
                },
                line_aerea:{
                    rangelength : "Ingresar 2 caracteres."
                },
                pasajero:{
                    minlength : 'Ingresar min. 5 caracteres.',
                    maxlength : 'Ingresar max. 100 caracteres.'
                },
                intinerario:{
                    required:false,
                    rangelength: 'Ingresar entre 1 a 11 caracteres.'
                },
                counter:{
                    minlength : 'Ingresar min. 5 caracteres.',
                    maxlength : 'Ingresar max. 100 caracteres.'
                },
            },
            // Do not change code below
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            }
        });
    }

    events.fn_validate_form_Busqueda_num_comprobante = function(){
        //$("#form-reporte-admin").validate().resetForm();
        $.validator.setDefaults({
            debug: true,
            success: "valid"
        });
        $("#form-reporte-admin").validate({
            rules: {
                nro_comprobante: {
                    required: true,
                    rangelength: [12, 12]
                },
                periodo_ini: {
                    required: false,
                    rangelength: [10, 10]
                },
                periodo_fin: {
                    required: false,
                    rangelength: [10, 10]
                },
                line_aerea:{
                    required: false,
                    rangelength: [2, 2]
                },
                pasajero:{
                    required:false,
                    minlength :5,
                    maxlength :100
                },
                intinerario:{
                    required:false,
                    rangelength:[1,11]
                },
                counter:{
                    required:false,
                    minlength :5,
                    maxlength :100
                },
            },
            messages: {
                nro_comprobante: {
                    required: 'Ingresar el N°. de Comprobante.',
                    rangelength : 'Ingresar 12 caracteres.'
                },
                periodo_ini: {
                    required: 'Ingresar la fecha inicio.',
                    rangelength : 'Formato de fecha invalido.'
                },
                periodo_fin: {
                    required: 'Ingresar la fecha fin.',
                    rangelength : 'Formato de fecha invalido.'
                },
                line_aerea:{
                    rangelength : "Ingresar 2 caracteres."
                },
                pasajero:{
                    minlength : 'Ingresar min. 5 caracteres.',
                    maxlength : 'Ingresar max. 100 caracteres.'
                },
                intinerario:{
                    required:false,
                    rangelength: 'Ingresar entre 1 a 11 caracteres.'
                },
                counter:{
                    minlength : 'Ingresar min. 5 caracteres.',
                    maxlength : 'Ingresar max. 100 caracteres.'
                },
            },
            // Do not change code below
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            }
        });
    }


    return {
      start: init
    };
  
  })();
  
  $(function(){
    reporte_adminitrativo.start();
  });
  