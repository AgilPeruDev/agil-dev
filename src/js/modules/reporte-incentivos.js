
/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

var reporte_incentivos = (function(){

    // Elementos del DOM
    var DOM = {
        form_reporte_incentivos :                   $(".form-reporte_incentivos"),
        form_reporte_incentivos :                   $("#form-reporte_incentivos"),
        content_filter          :                   $(".content-filter"),
        content_result          :                   $(".content-result"),
        droplist_sub_codigo     :                   $("#sub_codigo"),
        perido_rango            :                   $(".perido-rango"),
        frm_busqueda_rep_incentivo:                 $("#frm_busqueda_rep_incentivo"),
        frm_enviocorreo          :                  $("#frm_enviocorreo")
    };
  
    var init = function(){
        events.fn_consultar_rep_incentivos();
        events.fn_enviar_correo();
        events.init_dropdown();
        events.init_calendar();
    };
  
  
    var events = {};

    events.get_date_format =  function(years_last){
        var date      = new Date();
        var year      = date.getFullYear();
        var month     = date.getMonth();
        month         = month+1;
        month         = (month<10) ? '0'+month : month;
        var day       = date.getDate();
        day           = (day<10) ? '0'+day : day;
        var hoy       = day+'-'+month+'-'+year;
        var year_last = date.getFullYear()-years_last;
        var startDate = day+'-'+month+'-'+year_last;
        return {'hoy':hoy,'startDate':startDate}
    };
  
    events.init_calendar = function(){
        var periodo_ini     = DOM.perido_rango.find("[id='periodo_ini']");      
        var years_last_ini  = periodo_ini.data("years-last");   
        var rptas           = events.get_date_format(years_last_ini);
        var startDate       = rptas.startDate;
        var hoy             = rptas.hoy;
    
        periodo_ini.datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
          language: 'es',
          startDate: startDate,
          endDate: hoy
        }); 
        
      
        var periodo_fin     = DOM.perido_rango.find("[id='periodo_fin']");   
        var years_last_fin  = periodo_fin.data("years-last");  
        var rptas           = events.get_date_format(years_last_fin);
        var startDate       = rptas.startDate;
        var hoy             = rptas.hoy;    
    
        periodo_fin.datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
          language: 'es',
          startDate: startDate,
          endDate: hoy
        });  
  
    };

    events.val_next_to_atach_doc=function(e){
          e.preventDefault();
          var periodo_ini = DOM.perido_rango.find("[id='periodo_ini']");
          var periodo_fin = DOM.perido_rango.find("[id='periodo_fin']");
          var statusDate = true;
          
          if(periodo_ini.val()!=='' && periodo_fin.val() !== ''){
            var f_ini = periodo_ini.val();
            var f_fin = periodo_fin.val();
            f_ini = events.invertir_fecha(f_ini);
            f_fin = events.invertir_fecha(f_fin);          
            var per_ini = new Date(f_ini).getTime();
            var per_fin = new Date(f_fin).getTime();
            if(per_ini > per_fin){
                statusDate = false;
                alert("La Fecha Desde debe ser menor a la Fecha Hasta");
            }else{
              //DOM.atach_doc.fadeIn(500);
            }              
            //$(this).parent().parent().hide();
          }else{
            if(periodo_ini.val() == ''){
              periodo_ini.addClass("error");
              periodo_ini.focus();
            }
            if(periodo_fin.val() == ''){
              periodo_fin.addClass("error");
              periodo_fin.focus();
            }
        }

        return statusDate;
    }
    
    events.invertir_fecha =  function(fecha){
        var arrfecha = fecha.split("/");
        return arrfecha[2]+'/'+arrfecha[1]+'/'+arrfecha[0];
    }
  
    events.init_dropdown = function(){
        DOM.droplist_sub_codigo.ddslick();  
    } 

    events.fn_consultar_rep_incentivos = function(){
        $("#btnConsulta_rep_incentivos").click(function(e){
            events.fn_validate_form_Busqueda();
            if (!$("#frm_busqueda_rep_incentivo").valid()) {
                return;
            }else{
                var statusDate = events.val_next_to_atach_doc(e);
                if(statusDate){
                    events.load_table_rep_incentivo();
                    DOM.content_result.show();
                }
            }
        });

         //crearte mask  
         var input_periodo_ini = DOM.frm_busqueda_rep_incentivo.find('#periodo_ini');
         if(input_periodo_ini.length>0) 
             input_periodo_ini.inputmask('99/99/9999');
         var input_periodo_fin = DOM.frm_busqueda_rep_incentivo.find('#periodo_fin');
         if(input_periodo_fin.length>0) 
             input_periodo_fin.inputmask('99/99/9999');
         var input_nro_cotizacion = DOM.frm_busqueda_rep_incentivo.find('#cotizacion');
         if(input_nro_cotizacion.length>0) 
             input_nro_cotizacion.inputmask('Regex', {regex: "[0-9]{4}$"});
    };

    events.load_table_rep_incentivo = function(){

            var tablaHead = $("#table_data_rep_incentivo thead th");
            var tablaDatos = $("#table_data_rep_incentivo tbody tr");
            if(tablaHead.length>0 || tablaDatos.length>0){
                $("#table_data_rep_incentivo").DataTable().destroy();
                tablaHead.empty();
                tablaDatos.empty(); 
            }


        var dataHtmlComprobante = events.renderJson_comprobante();
        var dataHtmlHeaderComprobante = events.renderHeader_comprobante();
        var objet_table_comprobante_body = DOM.content_result.find('#table_data_rep_incentivo tbody');
        var objet_table_comprobante_header = DOM.content_result.find('#table_data_rep_incentivo thead');
        objet_table_comprobante_header.html(dataHtmlHeaderComprobante);
        objet_table_comprobante_body.html(dataHtmlComprobante);
        if(objet_table_comprobante_body.length>0){
            events.fn_ConfiguracionTabla('#table_data_rep_incentivo');
        }
    }
    events.renderHeader_comprobante = function(){
        var htmlheader = "";
        htmlheader = "<tr class='group-header'>";
        htmlheader += "  <th class='primary-header' colspan = '2'> Orden de servicio</th>";
        htmlheader += "  <th class='separador-table primary-header'></th>";
        htmlheader += "  <th class='primary-header' colspan = '5'> Datos de cotización</th>";
        htmlheader += "  <th class = 'separador-table primary-header'></th>";
        htmlheader += "  <th class='primary-header' colspan = '5'> Orden de Pago</th>";
        htmlheader += "</tr>";

        htmlheader += "<tr class='header-table'>";
        htmlheader += "  <th>N° de Orden</th>";
        htmlheader += "  <th>Fecha de Emisión</th>";
        htmlheader += "  <th class = 'separador-table'></th>";
        htmlheader += "  <th>Cotización</th>";
        htmlheader += "  <th>Counter DM</th>";
        htmlheader += "  <th>F. de Entrada</th>";
        htmlheader += "  <th>F. de Salida</th>";
        htmlheader += "  <th>Datos PAX</th>";
        htmlheader += "  <th class = 'separador-table'></th>";
        htmlheader += "  <th>Imp. Total</th>";
        htmlheader += "  <th>Imp. Pagado</th>";
        htmlheader += "  <th>Imp. Pendiente</th>";
        htmlheader += "  <th>Fecha</th>";
        htmlheader += "  <th>Incentivo</th>";
        htmlheader += "</tr>";

        return htmlheader;
    }

    events.renderJson_comprobante= function(){
        var AppenTableCompr = "";
        var jsondata = json_reporte_incentivo;
        for(var i=0; i<100; i++){
            AppenTableCompr += "<tr>";
            AppenTableCompr += "<td>" + jsondata[0].nro_orden + "</td>";
            AppenTableCompr += "<td>" + jsondata[0].fecha_emision + "</td>";
            AppenTableCompr += "<td class = 'separador-table'></td>";
            AppenTableCompr += "<td>" + jsondata[0].cotizacion + "</td>";
            AppenTableCompr += "<td>" + jsondata[0].counter_dm + "</td>";
            AppenTableCompr += "<td>" + jsondata[0].fecha_entrada + "</td>";
            AppenTableCompr += "<td>" + jsondata[0].fecha_salida + "</td>";
            AppenTableCompr += "<td>" + jsondata[0].datos_pax + "</td>";
            AppenTableCompr += "<td class = 'separador-table'></td>";
            AppenTableCompr += "<td>" + jsondata[0].imp_total + "</td>";
            AppenTableCompr += "<td>" + jsondata[0].imp_pagado + "</td>";
            AppenTableCompr += "<td>" + jsondata[0].imp_pendiente + "</td>";
            AppenTableCompr += "<td>" + jsondata[0].fecha + "</td>";
            AppenTableCompr += "<td>" + jsondata[0].incentivo + "</td>";
            AppenTableCompr += "</tr>";
        }
        return AppenTableCompr;       
    }

    events.fn_ConfiguracionTabla =function(idTabla){
        var tablaDatos = $(idTabla).DataTable(
            {
                "lengthChange": false,
                "pageLength": 10,
                "paging": true,
                "ordering": false,
                "info": false,
                "searching": false,
                "order": [],
                /*"scrollX": true,*/
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                "language": {
                    "lengthMenu": "Agrupar _MENU_ filas por página",
                    "zeroRecords": "No existen registros",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles.",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Filtrar:",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "»",
                        "previous": "«"
                    }
                }
                
            }
        );
        $(idTabla + ' tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                tablaDatos.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
    }

    events.fn_validate_form_Busqueda = function(){
        $.validator.setDefaults({
            debug: true,
            success: "valid"
        });
        $("#frm_busqueda_rep_incentivo").validate({
            rules: {
                periodo_ini: {
                    required: true,
                    rangelength: [10, 10],
                },
                periodo_fin: {
                    required: true,
                    rangelength: [10, 10]
                },
                cotizacion: {
                    required: false,
                    rangelength: [4, 4]
                }
            },
            messages: {
                periodo_ini: {
                    required: 'Ingresar la fecha Desde.',
                    rangelength : 'Formato fecha inválido.'
                },
                periodo_fin: {
                    required: 'Ingresar la fecha Hasta.',
                    rangelength : 'Formato fecha inválido.'
                },
                cotizacion:{
                    required: 'Ingresar el Nro. de cotización.',
                    rangelength : 'Ingresar 4 caracteres.',
                },
            },
            // Do not change code below
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            }
        });
    }

    events.fn_validate_form_enviocorreo = function(){
        $.validator.setDefaults({
            debug: true,
            success: "valid"
        });
        $("#frm_enviocorreo").validate({
            rules: {
                email: {
                    required: true,
                    rangelength: [5, 50],
                    email: true
                }
            },
            messages: {
                email: {
                    required: 'Ingresar el correo.',
                    rangelength : 'Correo Inválido.',
                    email: "Correo Inválido."
                }
            },
            // Do not change code below
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            }
        });
    }
    events.fn_enviar_correo = function(){
        $("#btnEnviarCorreo").click(function(){
            events.fn_validate_form_enviocorreo();
            if (!$("#frm_enviocorreo").valid()) {
                return;
            }else{
                $("#success_rep_incentivo").modal('show');
                $("#email").val("");
            }
        });
        var input_intinerario = DOM.frm_enviocorreo.find('#email');
         if(input_intinerario.length>0) 
            input_intinerario.inputmask('Regex', {regex: "[A-Za-z0-9@._]{5,50}$"});

    }

    return {
      start: init
    };
  
  })();
  
  $(function(){
    reporte_incentivos.start();
  });


  var json_reporte_incentivo = [
    {
		"nro_orden": "118470",
		"fecha_emision": "15/06/2019",
		"cotizacion": "113122",
		"counter_dm": "Mirian Segura",
        "fecha_entrada": "08/04/2019",
        "fecha_salida" : "08/04/2019",
        "datos_pax" : "Jaime Medina Perez",
        "imp_total":"$340.00",
        "imp_pagado": "$240.00",
        "imp_pendiente" : "$100.00",
        "fecha" : "18/04/2019",
        "incentivo" : "Tarjeta Mágica"
    }
  ];
  
