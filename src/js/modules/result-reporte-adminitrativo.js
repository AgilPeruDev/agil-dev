/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

var reporte_adminitrativo = (function(){

    // Elementos del DOM
    var DOM = {
      form_reporte_admin     :    $('#form-reporte-admin'), 
      periodo :                   $(".perido-rango"),
      form_tabla_comprobantes    :$('.form-tabla-comprobantes'),
      form_tabla_boletos         :$('.form-tabla-boletos'),
      droplist_sub_codigo :      $("#sub_codigo"),
      droplist_forma_pago :      $("#forma_pago"),
    };
  
    var init = function(){
      events.load_table_comprobante();
      events.load_table_boletos();
      events.toogle_default();
      events.fn_eventClickButton();
      events.fn_eventClikButtonRegresar();
    };
  
  
    var events = {};

    events.toogle_default = function(){
        $(".form-tabla-comprobantes").show();
        $(".form-tabla-boletos").hide();
    }

    events.load_table_comprobante = function(){
        var dataHtmlComprobante=events.renderJson_comprobante();
        var objet_table_comprobante_body = DOM.form_tabla_comprobantes.find('#table_data_comprobantes tbody');
        objet_table_comprobante_body.html(dataHtmlComprobante);
        if(objet_table_comprobante_body.length>0){
            events.fnConfigureDatatable('#table_data_comprobantes');
        }
    }

    events.load_table_boletos = function(){ 
        var dataHtmlBoletos=events.renderJson_boletos();
        var objet_table_boletos_body = DOM.form_tabla_boletos.find('#table_data_boletos tbody');
        objet_table_boletos_body.html(dataHtmlBoletos);
        if(objet_table_boletos_body.length>0){
            events.fnConfigureDatatable('#table_data_boletos');
        }
    }

    events.fn_eventClikButtonRegresar = function(){
        $("#btn_regresar").click(function(){
            document.location='./reporte-administrativo.html';
        });
    }

    events.fn_eventClickButton = function(){
        $("#tab-comprobante").click(function(){
            $(".form-tabla-comprobantes").show();
            $(".form-tabla-boletos").hide();
            $("#tab-comprobante").removeClass('tabs--normal').addClass('tabs--resaltar'); 
            $("#tab-boleto").removeClass('tabs--resaltar').addClass('tabs--normal'); 
        });
        
        $("#tab-boleto").click(function(){
            $(".form-tabla-comprobantes").hide();
            $(".form-tabla-boletos").show();
            $("#tab-boleto").removeClass('tabs--normal').addClass('tabs--resaltar');
            $("#tab-comprobante").removeClass('tabs--resaltar').addClass('tabs--normal');
        });
       
    }

    events.renderJson_comprobante= function(){
        var AppenTableCompr = "";
        var jsondata = Array_comprobantes;
        var countador = 0;
        for(var i=0; i<jsondata.length; i++){
            countador = i+1;
            AppenTableCompr += "<tr>"
            AppenTableCompr += "<td>" + countador+ "</td>";
            AppenTableCompr += "<td>" + jsondata[i].fecha_emision + "</td>";
            AppenTableCompr += "<td>" ;
            AppenTableCompr += "    <a href='#'>" + jsondata[i].tipo_numero + "</a>";
            AppenTableCompr += "</td>" ;
            AppenTableCompr += "<td>" + jsondata[i].moneda + "</td>";
            AppenTableCompr += "<td>" + jsondata[i].a_pagar + "</td>";
            AppenTableCompr += "<td>" + jsondata[i].pendiente_pago + "</td>";
            AppenTableCompr += "<td>" + jsondata[i].vencimiento + "</td>";
            AppenTableCompr += "<td>" + jsondata[i].descuento + "</td>";
            AppenTableCompr += "<td>" + jsondata[i].facturar_comision + "</td>";
            AppenTableCompr += "<td>" + jsondata[i].estado_comprobante + "</td>";
            AppenTableCompr += "</tr>";
        }
        return AppenTableCompr;
    }

    events.renderJson_boletos= function(){
        var AppenTableBol = "";
        var jsondata = Array_boletos;
        var countador = 0;
        for(var i=0; i<jsondata.length; i++){
            countador = i+1;
            AppenTableBol += "<tr>"
            AppenTableBol += "<td>" + countador +"</td>";
            AppenTableBol += "<td>";
            AppenTableBol += "  <a href='#'>"+ jsondata[i].nro_ticket +"</a>";
            AppenTableBol += "</td>"
            AppenTableBol += "<td>";
            AppenTableBol += " <a href='#'><span class='glyphicon glyphicon-list-alt'></span></a>";
            AppenTableBol += "</td>";
            AppenTableBol += "<td>" + jsondata[i].prov_Linea_aerea + "</td>";
            AppenTableBol += "<td>" + jsondata[i].pasajero + "</td>";
            AppenTableBol += "<td>" + jsondata[i].fecha_emision + "</td>";
            AppenTableBol += "<td>" + jsondata[i].itinerario + "</td>";
            AppenTableBol += "<td>" + jsondata[i].pnr + "</td>";
            AppenTableBol += "<td>" + jsondata[i].tarifa_neta + "</td>";
            AppenTableBol += "<td>" + jsondata[i].descuento + "</td>";
            AppenTableBol += "<td>" + jsondata[i].forma_pago + "</td>";
            AppenTableBol += "<td>" + jsondata[i].entrego_cccf + "</td>";
            AppenTableBol += "<td>" + jsondata[i].counter + "</td>";
            AppenTableBol += "</tr>";
        }
        return AppenTableBol;
    }

    events.fnConfigureDatatable =function(idTabla){
        var tablaDatos = $(idTabla).DataTable(
            {
                "lengthChange": false,
                "pageLength": 10,
                "paging": true,
                "ordering": false,
                "info": false,
                "searching": false,
                "order": [],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                "language": {
                    "lengthMenu": "Agrupar _MENU_ filas por página",
                    "zeroRecords": "No existen registros",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles.",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Filtrar:",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "»",
                        "previous": "«"
                    }
                }
                
            }
        );
        $(idTabla + ' tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                tablaDatos.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
    }

    return {
      start: init
    };
  
  })();
  
  $(function(){
    reporte_adminitrativo.start();
  });
  


  var Array_comprobantes = [
	{
		"fecha_emision": "03/06/2019",
		"tipo_numero": "DC0000023456",
		"moneda": "USD",
		"a_pagar": "1122",
        "pendiente_pago": "390",
        "vencimiento" : "11/09/2019",
        "descuento":"34.59",
        "facturar_comision": "No",
        "estado_comprobante" : ""
    },
    {
		"fecha_emision": "03/06/2019",
		"tipo_numero": "DC0000023456",
		"moneda": "USD",
		"a_pagar": "54365",
        "pendiente_pago": "390",
        "vencimiento" : "11/09/2019",
        "descuento":"34.59",
        "facturar_comision": "Sí",
        "estado_comprobante" : ""
    },
    {
		"fecha_emision": "03/06/2019",
		"tipo_numero": "DC0000023456",
		"moneda": "USD",
		"a_pagar": "1234",
        "pendiente_pago": "390",
        "vencimiento" : "11/09/2019",
        "descuento":"34.59",
        "facturar_comision": "No",
        "estado_comprobante" : "Anulado"
    },
    {
		"fecha_emision": "03/06/2019",
		"tipo_numero": "DC0000023456",
		"moneda": "USD",
		"a_pagar": "1224",
        "pendiente_pago": "390",
        "vencimiento" : "11/09/2019",
        "descuento":"34.59",
        "facturar_comision": "No",
        "estado_comprobante" : "Anulado"
    },
    {
		"fecha_emision": "03/06/2019",
		"tipo_numero": "DC0000023456",
		"moneda": "USD",
		"a_pagar": "3243",
        "pendiente_pago": "390",
        "vencimiento" : "11/09/2019",
        "descuento":"34.59",
        "facturar_comision": "Sí",
        "estado_comprobante" : "Anulado"
    },
    {
		"fecha_emision": "03/06/2019",
		"tipo_numero": "DC0000023456",
		"moneda": "USD",
		"a_pagar": "3546",
        "pendiente_pago": "390",
        "vencimiento" : "11/09/2019",
        "descuento":"34.59",
        "facturar_comision": "Sí",
        "estado_comprobante" : "Anulado"
    },
    {
		"fecha_emision": "03/06/2019",
		"tipo_numero": "DC0000023456",
		"moneda": "USD",
		"a_pagar": "6767",
        "pendiente_pago": "390",
        "vencimiento" : "11/09/2019",
        "descuento":"34.59",
        "facturar_comision": "Sí",
        "estado_comprobante" : "Anulado"
    },
    {
		"fecha_emision": "03/06/2019",
		"tipo_numero": "DC0000023456",
		"moneda": "USD",
		"a_pagar": "34466",
        "pendiente_pago": "390",
        "vencimiento" : "11/09/2019",
        "descuento":"34.59",
        "facturar_comision": "Sí",
        "estado_comprobante" : ""
    },
    {
		"fecha_emision": "03/06/2019",
		"tipo_numero": "DC0000023456",
		"moneda": "USD",
		"a_pagar": "56673",
        "pendiente_pago": "390",
        "vencimiento" : "11/09/2019",
        "descuento":"34.59",
        "facturar_comision": "No",
        "estado_comprobante" : ""
    },
    {
		"fecha_emision": "03/06/2019",
		"tipo_numero": "DC0000023456",
		"moneda": "USD",
		"a_pagar": "777856",
        "pendiente_pago": "390",
        "vencimiento" : "11/09/2019",
        "descuento":"34.59",
        "facturar_comision": "Sí",
        "estado_comprobante" : "Anulado"
    },
    {
		"fecha_emision": "03/06/2019",
		"tipo_numero": "DC0000023456",
		"moneda": "USD",
		"a_pagar": "77644",
        "pendiente_pago": "390",
        "vencimiento" : "11/09/2019",
        "descuento":"34.59",
        "facturar_comision": "Sí",
        "estado_comprobante" : ""
    },
    {
		"fecha_emision": "03/06/2019",
		"tipo_numero": "DC0000023456",
		"moneda": "USD",
		"a_pagar": "55456",
        "pendiente_pago": "390",
        "vencimiento" : "11/09/2019",
        "descuento":"34.59",
        "facturar_comision": "Sí",
        "estado_comprobante" : "Anulado"
    },
    {
		"fecha_emision": "03/06/2019",
		"tipo_numero": "DC0000023456",
		"moneda": "USD",
		"a_pagar": "78855",
        "pendiente_pago": "390",
        "vencimiento" : "11/09/2019",
        "descuento":"34.59",
        "facturar_comision": "Sí",
        "estado_comprobante" : "Anulado"
    },
    {
		"fecha_emision": "03/06/2019",
		"tipo_numero": "DC0000023456",
		"moneda": "USD",
		"a_pagar": "4345554",
        "pendiente_pago": "390",
        "vencimiento" : "11/09/2019",
        "descuento":"34.59",
        "facturar_comision": "Sí",
        "estado_comprobante" : ""
	},
  ];

  var Array_boletos = [
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    },
    {
        "nro_ticket": "1240137456",
        "voucher" : "",
        "prov_Linea_aerea": "21",
        "pasajero":"SANCHEZ/HUGO",
        "fecha_emision":"14/06/2019",
        "itinerario":"LIM-CUZ-LIM",
        "pnr":"ZUPCJY",
        "tarifa_neta": "$128",
        "descuento":"0",
        "forma_pago":"CASH",
        "entrego_cccf": "-",
        "counter": "MOTOR WEB KIU"
    }
  ];
  