/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

var service_to_customer = (function(){

  // Instancia de funciones externas
  valida_ddslick = new valida_ddslick("#form-serv-cli");

  // Elementos del DOM
  var DOM = {
    form_serv_cli: $('#form-serv-cli')  
  };

  var init = function(){
    events.validation();
    events.open_other_subject();
    events.init_dropdown();
    events.load_filter();
    events.valida_ddclick();
  };

  var events = {};

  events.scrollTop_error =  function(){
    var input_errores = DOM.form_serv_cli.find("input.error,.dd-select--error");
    var ele = input_errores[0];
    var top = $(ele).offset().top;
    var header_h = $("header").outerHeight();
    top = top - header_h - 50;
    $('body, html').animate({
      scrollTop: top+'px'
    }, 500);

  };

  events.valida_ddclick =  function(){
    
    DOM.form_serv_cli.find("button[type='submit']").on('click',function(){
      valida_ddslick.init();  
    });
  };

  events.open_other_subject = function(){
    DOM.form_serv_cli.find('.subject_service').on('change',function(){
      var elem          = $(this);
      var val           = elem.val();
      var box           = elem.data('input-open');
      
      DOM.form_serv_cli.find(".form-serv-cli__detail").hide();
      $(box).show();      
    });
  }

  events.validation = function(){
    DOM.form_serv_cli.validate({
      success: function(form){
      },
      onSubmit: function(element) {
        $(element).valid();
      },
      submitHandler: function(form) {
        if(valida_ddslick.init()>0){
          return false;
        }else{        
          $('#modal_success_service_customer').modal('show');
        }
      }
    });

    DOM.form_serv_cli.find("input#nombre").inputmask('Regex', {regex: "[A-Za-z ]{1,50}$"}); 
    DOM.form_serv_cli.find("input#apellido").inputmask('Regex', {regex: "[A-Za-z ]{1,50}$"}); 
    DOM.form_serv_cli.find("input#agencia").inputmask('Regex', {regex: "[A-Za-z&$#.0-9 ]{1,100}$"});
    DOM.form_serv_cli.find("input#telefono").inputmask('Regex', {regex: "[0-9]*"});
    DOM.form_serv_cli.find("input#cargo").inputmask('Regex', {regex: "[0-9a-zA-Z ]{1,40}$"}); 

    
    DOM.form_serv_cli.find("input#confimar_correo").rules("add", {
      equalTo: "#correo",
      messages :{
        equalTo: "La confirmación de su correo no es válida"
      }
    }); 
   
  };  
    
  events.load_filter = function(){
    var dataList = document.querySelector('#list-nombre-personal'),
    input = document.querySelector('#nombre-personal');
    var request = new XMLHttpRequest();
    request.onreadystatechange = function(response) {
    if (request.readyState === 4) {
      if (request.status === 200) {
        var jsonOptions = JSON.parse(request.responseText);
        jsonOptions.forEach(function(item) {
          var option = document.createElement('option');
          option.value = item;
          dataList.appendChild(option);
        });
        input.placeholder = "Escriba el nombre";
      } else {
        input.placeholder = "Error al cargar lista";
      }
    }
    };

    input.placeholder = "Escribe el nombre";
    request.open('GET', './data/nombres.json', true);
    request.send();

    // $("#aerolineas").change(function(){
    //   $(".iframe-pdf-comision").removeClass("iframe-pdf-comision--hide");
    //   $(".opciones-pdf").removeClass("opciones-pdf--hide");
    // });
  }

  events.init_dropdown = function(){
    DOM.form_serv_cli.find('#droplist_oficinas').ddslick({
      onSelected: function(data){
        var value = data.selectedData.value;
        if(value!=0){
          var id = data.original.context.id;
          var t = $("#"+id);
          t.find('div.dd-select').removeClass('dd-select--error');
          t.find('ul.dd-options').removeClass('dd-options--error');
          t.parent().parent().find('label.Label--error').remove();
        }
      } 
    }); 
    DOM.form_serv_cli.find('#droplist_areas').ddslick({
      onSelected: function(data){
        var value = data.selectedData.value;
        if(value!=0){
          var id = data.original.context.id;
          var t = $("#"+id);
          t.find('div.dd-select').removeClass('dd-select--error');
          t.find('ul.dd-options').removeClass('dd-options--error');
          t.parent().parent().find('label.Label--error').remove();
          
        }
      } 
    });    
  } 



  return {
    start: init
  };

})();

$(function(){
  service_to_customer.start();
});

