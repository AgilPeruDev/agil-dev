/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

var standar = (function(){

  // Elementos del DOM
  var DOM = {
    body:                       $('body'),
    header:                     $('header'),
    main:                       $('main'),
    button_menu_mobile:         $('.menu__button-burger'),
    button_close_menu:         $('.menu__button-close'),
    items_menu_mobile:          $('.menu__items'),
    form_login :                $('#form-login'),
    menu_toogle :               $('.data-session-on__toogle'),
    input_file:                 $("") 
  };

  var init = function(){
    events.toggle_menu_mobile();
   // events.check_browser();
    events.validation_login();
    events.load_menu_toogle();
    events.change_pass_to_text();
    events.up_popover();
    events.load_menu_toogle_main();
  };

  

  var events = {};


  
  events.load_menu_toogle_main = function(){

      
       
        $("ul.ul_menu > li > a").on('click',function(e){  
          var w = $(window).width();  
          if(w<=1300){    
            
            var t = $(this); 
            if(t.parent("li").hasClass("sub-menu")){           
              e.preventDefault();
              if(t.parent("li").find('>ul').is(':visible')){
                t.parent("li").find('>ul').slideToggle('slow');
                t.parent("li").removeClass('sub-menu--open');
              }else{
                var as =$("ul.ul_menu li > ul");
                $.each(as,function(i,v){
                  var ul = $(v);
                  if (ul.is(':visible')){
                    ul.slideToggle('slow');
                    ul.parent("li").removeClass('sub-menu--open');
                  }
                })
                t.parent("li").find('>ul').slideToggle('slow');
                t.parent("li").addClass('sub-menu--open');
              }              
            }else{

            }
          }
          else{
            console.log("eeded");
            $("ul.ul_menu > li > ul").css("display","none");
          }          
        });


  };

  events.up_popover = function(){
    $('[data-toggle="popover"]').popover();
  }

  events.change_pass_to_text = function(){
    var i = $('.form__passw i');
    i.on('click',function(){
      $(this).toggleClass("icon-eye-off");
      var inn = $(this).parent().find('input');
      if(inn.prop('type') == "password"){
        inn.prop('type','text');
      }else{
        inn.prop('type','password');
      };
    })
  }
  

  events.lister_scroll_header_fixed = function(){
    var h = DOM.header.outerHeight();
    DOM.body.css('padding-top',h+'px');    
    $(window).resize(function() { 
      var h = DOM.header.outerHeight();
      DOM.body.css('padding-top',h+'px');
    });
  }
  
  events.load_menu_toogle = function(){    
    
    DOM.menu_toogle.on('click',function(){
      if($(this).parent().hasClass('open')){
        $(this).parent().removeClass('open');
      }else{
        $(this).parent().addClass('open');        
      }
    });
  };

  events.validation_login = function(){
    DOM.form_login.validate({
      success: function(form){
      },
      onSubmit: function(element) {
        $(element).valid();
      },
      submitHandler: function(form) {
        form.submit();
      }
    });
    if($("input[name='password']").length>0){
      DOM.form_login.find("input[name='password']").inputmask('Regex', {regex: "[A-Za-z0-9$@!]{1,15}$"}); 
    }
  };
 
    

  events.init_dropdown = function(){
    
    DOM.droplist_agency_country.ddslick({
      onOpen: function(data){
        setTimeout(function () {
          scroll_ayment_method.refresh();
        }, 200);
        
      }
    });  
    var scroll_ayment_method = new IScroll('#agency_country .dd-options', {
			mouseWheel: true,
			scrollbars: true,
			interactiveScrollbars: true,
			click: true
    });

    DOM.droplist_agency_city.ddslick(); 
    DOM.droplist_agency_district.ddslick(); 
    DOM.droplist_agency_incentive.ddslick(); 
    DOM.droplist_type_document_agency.ddslick(); 
    DOM.droplist_user_district.ddslick();  
    DOM.droplist_user_city.ddslick();  
    DOM.droplist_user_country.ddslick();    
    DOM.droplist_type_document_user.ddslick();    
    
  } 
  events.lister_scroll_header_fixed = function(){
    var h = DOM.header.outerHeight();
    DOM.body.css('padding-top',h+'px');    
    $(window).resize(function() { 
      var h = DOM.header.outerHeight();
      DOM.body.css('padding-top',h+'px');
    });
  }

  events.check_browser = function(){
    if ( is.ie(6) || is.ie(7) || is.ie(8)) {
      console.log("IE6-IE7-IE8");
    }else if(is.chrome()){
      console.log("Browser-CHROME");
    }else if(is.firefox()){
      console.log("Browser-FIREFOX");
    }else{
      console.log('other-navegador');
    }
  }
  events.init_main_banner=function(){

    if(DOM.main_banner){
      DOM.main_banner.slick({
        dots: true,
        infinite: true,
        arrows: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        pauseOnHover: false,
        autoplaySpeed: 2000,
      });
    }
  }

  events.init_carousel_dm_products=function(){
    DOM.carousel_dm_products.slick({
      dots: true,
      infinite: true,
      slidesToShow: 4,
      autoplay: true,
      autoplaySpeed: 1000,
      slidesToScroll: 1,
      swipeToSlide:true,
      arrows: false,
      responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '40px',
              slidesToShow: 3
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '40px',
              slidesToShow: 1
            }
          }
        ]
    });    
  }  

  
  events.toggle_menu_mobile = function(){
    $(document).on('click','.menu__button-close',function(){
      $(this).removeClass("menu__button-close--open");
      setTimeout(function(){
        DOM.button_menu_mobile.removeClass('menu__button-burger--open');
        DOM.items_menu_mobile.removeClass('menu__items--open');
        DOM.body.removeClass('header--fixed');        
      }, 200);
      
    });
    DOM.button_menu_mobile.on('click',function(){
      if($('.menu__button-burger.menu__button-burger--open').length>0){
      }else{
        DOM.button_menu_mobile.addClass('menu__button-burger--open');
        DOM.items_menu_mobile.addClass('menu__items--open');
        DOM.button_close_menu.addClass('menu__button-close--open');
        DOM.body.addClass('header--fixed');
      }
    });
  };

  return {
    start: init
  };

})();

$(function(){
  standar.start();
});


var valida_ddslick = function(form) {
  var dom = {};
  var events = {};
  var exports = {};

  exports.init = function() {
	  catchDom();
    return events.search();
  };

  var catchDom = function() {
    dom.parent         = $(form);
  };

  events.scrollTop_error =  function(){
    var input_errores = dom.parent.find("input.error,.dd-select--error");
    if (input_errores.length==0) {return 0;}else{
      var ele = input_errores[0];
      var top = $(ele).offset().top;
      var header_h = $("header").outerHeight();
      top = top - header_h - 50;
      $('body, html').animate({
        scrollTop: top+'px'
      }, 500);
      return input_errores.length;
    }
  };

  events.search = function() {
    var dd_container = dom.parent.find(".dd-container");
    $.each(dd_container, function( index, value ) {
      var t = $(this);
      var val = t.find('.dd-selected-value').val();
      if(val == 0){
        if($(".dd-container:eq('"+index+"')").closest('.Select').data()){
          if($(".dd-container:eq("+index+")").closest('.Select').data('ruleRequired') && 
            $(".dd-container:eq("+index+")").closest('.Select').data('msgRequired')!=null){
             
              t.find('div.dd-select').addClass('dd-select--error');
              t.find('ul.dd-options').addClass('dd-options--error');
              var str = '<label class="Label--error">'+ $(".dd-container:eq("+index+")").closest('.Select').data('msgRequired')+ '</label>';
              if(t.parent().parent().append().find('.Label--error').length>0){
              }else{
                t.parent().parent().append(str);
              } 
          }
        }
      }
     });
     var num = events.scrollTop_error();
     return num;
  };  

  return exports;
};
